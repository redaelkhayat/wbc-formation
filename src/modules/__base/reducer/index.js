import { ADD_PERSON, REMOVE_PERSON } from "./actionTypes";

const initialState = {
    data: []
};

export default (state = initialState, action) => {
    switch (action.type) {
      case ADD_PERSON: {
        return {
            ...state,
            data: [
                ...state.data,
                action.person
            ]
        }
        break;
      }

      case REMOVE_PERSON: {
        return {
            ...state,
            data: state.data.filter(person => {
                return person.id != action.id;
            })
        }
        break;
      }

      default:
        return state;
        break;
    }
}