import {ADD_PERSON, REMOVE_PERSON} from "./actionTypes";

/**
 * Add new person
 * @param {Object} person 
 */
export const add = person => {
    return {
        type: ADD_PERSON,
        person: person
    };
};

/**
 * Remove a person
 * @param {number} id 
 */
export const remove = id => {
    return {
        type: REMOVE_PERSON,
        id: id
    };
};
