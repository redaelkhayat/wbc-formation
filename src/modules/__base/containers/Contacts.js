import React from "react";
import ListContacts from "../components/ListContacts";

import {add, remove} from "../reducer/actions";

import {connect} from "react-redux";

class Contacts extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      active: null
    };

    props.add({ id: 1, name: "John" });
    props.add({ id: 2, name: "Doe" });
    props.add({ id: 3, name: "Foo" });
    props.add({ id: 4, name: "Bar" });
  }
  componentWillMount() {
    if (this.props.persons) {
      this.setState({
        persons: this.props.persons
      });
    }
  }
  removePerson(id) {
    this.props.remove(id);
  }
  activate(index) {
    this.setState({
        active: index
    });
  }
  render() {
    const {active} = this.state;
    return (
      <ListContacts
        persons={this.props.persons}
        active={active}
        remove={this.removePerson.bind(this)}
        activate={this.activate.bind(this)} />
    );
  }
}

const mapStateToProps = (state) => {
    return {
        persons: state.persons.data
    };
};

const mapDispatchToProps = {
    add,
    remove
};

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);
