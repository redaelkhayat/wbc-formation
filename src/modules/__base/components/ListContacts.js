import React from 'react';
import Person from "../../common/Person";

const ListContacts = props => {
    return <div>
        <h1>WB&amp;C Contacts</h1>
        {props.persons.map((person, index) => {
          const isActive = props.active == index;
          return <div key={person.id}>
              <Person
                {...person}
                onRemove={() => { props.remove(person.id) }}
                active={isActive}
              />
              <button onClick={() => { props.activate(index) }}>
                activate
              </button>
            </div>;
        })}
      </div>;
};

export default ListContacts;