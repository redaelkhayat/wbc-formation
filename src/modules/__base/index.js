import React from 'react';

import Contacts from "./containers/Contacts";

class Home extends React.Component {
    render(){
        return <Contacts />;
    }
}

export default Home;