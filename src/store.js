import { createStore, combineReducers, applyMiddleware } from "redux";
import logger from "redux-logger";

/** import des reducers */

const store = createStore(
  combineReducers({
    // reducers go here
  }),
  applyMiddleware(logger)
);
export { store };
